package storm.starter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class DataExtractor {

	private File file;
	BufferedReader br;

	// TODO: do not use scanner....
	public DataExtractor(String filePath) throws FileNotFoundException {
		this.file = new File(filePath);
		this.br = new BufferedReader(new FileReader(file));
	}

	/**
	 * Example tweet
	 * (UserID\tTweetID\tLatitude\tLongitude\tCreatedAt\tText\tPlaceID):
	 * 41418752 19395304894 38.929854 -77.027976 2010-07-24 04:25:41 I'm at The
	 * Wonderland Ballroom (1101 Kenyon St NW, at 11th St, Washington, DC) w/ 2
	 * others. http://4sq.com/2z5p82 8dc80c56f429dd1e
	 */
	public Tweet readTweet() throws ParseException {
		String line = null;
		try {

			while ((line = br.readLine()) != null) {
				Tweet tweet = processLine(line);
				if (tweet != null){
					return tweet;
				}
				
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private Tweet processLine(String checkin) throws ParseException {

		String[] splitedCheckin = checkin.split("\t");
		if (splitedCheckin.length < 6)
			return null;
		String userID = splitedCheckin[0];
		String tweetID = splitedCheckin[1];
		double latitude = Double.parseDouble(splitedCheckin[2]);
		double longitude = Double.parseDouble(splitedCheckin[3]);
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = (Date) (formatter.parse(splitedCheckin[4]));
		String text = splitedCheckin[5];
		// String placeID = scanner.next();
		return new Tweet(text, latitude, longitude, userID, date, tweetID);

	}

	public int countWords(Tweet tweet) {
		// System.out.println("Tweet: "+tweet.getTweet());
		String[] words = tweet.getTweet().split(" ");
		// System.out.println("\tSize:"+words.length);
		return words.length;
	}

}
