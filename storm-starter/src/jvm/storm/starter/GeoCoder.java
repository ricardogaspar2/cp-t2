package storm.starter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;

import org.w3c.dom.Document;
import org.xml.sax.*;
import javax.xml.parsers.*;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

public class GeoCoder {
	private static String url;
	private StringBuilder string;

	public GeoCoder() {
		// TODO Auto-generated constructor stub
	}

	public String getCountryName(double lat, double lon) {
		String xml = null;
		String result = "";
		buildURL(lat, lon);

		try {
			URLConnection con = new URL(url).openConnection();
			InputStream is = con.getInputStream();
			xml = getStringFromInputStream(is);

			InputSource source = new InputSource(new StringReader(xml));

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document document = db.parse(source);

			XPathFactory xpathFactory = XPathFactory.newInstance();
			XPath xpath = xpathFactory.newXPath();

			result = xpath.evaluate("/geonames/country/countryName", document);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	private void buildURL(double lat, double lon) {
		string = new StringBuilder();
		string.append("http://ws.geonames.org/countryCode?");
		string.append("lat=" + lat);
		string.append("&lng=" + lon);
		string.append("&type=xml");
		// url = "http://ws.geonames.org/countryCode?lat=49.03&lng=10.2";
		url = string.toString();

	}

	private String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}
}
