package storm.starter;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.task.ShellBolt;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import storm.starter.spout.*;
import storm.trident.operation.builtin.Sum;

import java.io.File;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * This topology demonstrates Storm's stream groupings and multilang capabilities.
 */

public class WordCountTweet {
	
	volatile static boolean regionBoltStopped = false;
	volatile static boolean wordCountBoltStopped = false;
	private final static Map<String, String> keywords;
	static {
		Map<String, String> temp = new HashMap<String, String>();
		temp.put("foursquare", "foursquare");
		temp.put("airport", "airport");
		temp.put("home", "home");
		temp.put("house", "house");
		temp.put("city", "city");
		temp.put("jakarta", "jakarta");
		temp.put("hotel", "hotel");
		temp.put("cafe", "cafe");
		temp.put("bar", "bar");
		temp.put("coffee", "coffee");
		temp.put("club", "club");
		temp.put("beach", "beach");
		temp.put("grill", "grill");
		temp.put("lake", "lake");
		temp.put("chicago", "chicago");
		temp.put("church", "church");
		temp.put("dinner", "dinner");
		temp.put("iphone", "iphone");
		temp.put("college", "college");
		temp.put("apple", "apple");
		temp.put("austin", "austin");
		temp.put("food", "food");
		temp.put("indonesia", "indonesia");
		temp.put("bank", "bank");
		temp.put("broadway", "broadway");
		temp.put("friends", "friends");
		temp.put("birthday", "birthday");
		temp.put("amsterdam", "amsterdam");
		temp.put("atlanta", "atlanta");
		temp.put("bangkok", "bangkok");
		temp.put("hospital", "hospital");
		temp.put("campus", "campus");
		temp.put("breakfast", "breakfast");
		temp.put("android", "android");
		temp.put("ios", "ios");
		temp.put("windows", "windows");
		temp.put("nokia", "nokia");
		temp.put("instagram", "instagram");
		temp.put("facebook", "facebook");
		temp.put("work", "work");
		temp.put("microsoft", "microsoft");
		temp.put("google", "google");
		temp.put("railway", "railway");
		temp.put("berlin", "berlin");

		keywords = Collections.unmodifiableMap(temp);
	}

	/**
	 * Nosso SplitSentence em java. Responsável por partir a frase do tweet em
	 * palavras. Emite palavra, tweet
	 * 
	 */
	public static class SplitSentence extends BaseBasicBolt {

		@Override
		public void execute(Tuple tuple, BasicOutputCollector collector) {
			Tweet tweet = (Tweet) tuple.getValue(0);
			if (tweet == null) {
				collector.emit(new Values(null, null, null, null));

			} else {
				String sentence = tweet.getTweet();
				String[] words = sentence.split(" ");
				for (int i = 0; i < words.length; i++) {

					if (keywords.get(words[i].toLowerCase()) != null)
						collector.emit(new Values(words[i], tweet.getDate(),
								tweet.getLatitude(), tweet.getLongitude()));

				}
			}
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("word", "date", "lat", "long"));
		}
	}

	public static class SplitWordsByDate extends BaseBasicBolt {

		@Override
		public void execute(Tuple tuple, BasicOutputCollector collector) {

			if (tuple.getValue(0) == null) {
				collector.emit(new Values(null, null, null));
			} else {

				Date date = (Date) tuple.getValue(1);
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String dateStrings[] = df.format(date).split("-");
				int year = Integer.parseInt(dateStrings[0]);
				int month = Integer.parseInt(dateStrings[1]);

				collector.emit(new Values(tuple.getValue(0), year, month));
			}
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("word", "year", "month"));
		}
	}

	/**
	 * Conta as palavras por ano. Tem um hashmap que guarda as contagens mensais
	 * de uma palavra ao longo de um dado ano. No final faz dump para um
	 * ficheiro.
	 */
	public static class CountWordsByYear extends BaseBasicBolt {
		//Mapa para a contagem das palavras ao longo do ano
		Map<String, int[]> wordYearlyCount = new HashMap<String, int[]>();
		//Mapa para a contagem anual de cada palavra. Cada ano tem o seu nr de palavras
		Map<Integer, Map<String, Integer>> wordYearlySum = new HashMap<Integer, Map<String,Integer>>();

		//Ordena um mapa por valores por forma a fazer um ranking das palavra mais usadas
		class MapComparator implements Serializable, Comparator {
			Map map = null;

			MapComparator(Map map) {
				this.map = map;
			}

			@Override
			public int compare(Object o1, Object o2) {
				Integer value1 = (Integer) map.get(o1);
				Integer value2 = (Integer) map.get(o2);
				int result = value1.compareTo(value2) * -1;

				if (result == 0 && !o1.equals(o2))
					return 1;

				return result;
			};
		}

		boolean isFileWritten = false;
		PrintWriter writer;

		@Override
		public void execute(Tuple tuple, BasicOutputCollector collector) {

			if (tuple.getValue(0) == null) {
				if (!isFileWritten) {
					try {
						writer = new PrintWriter("output.csv", "UTF-8");
					} catch (Exception e) {
					}

					// Ordena todos os valores, de forma a obter a lista
					// ordenada
					// por palavra mais popular.

					writer.println("sep=,");
					writer.println("Word" + "," + "Year" + "," + "Total");

					Iterator<Integer> it = wordYearlySum.keySet().iterator();
					while (it.hasNext()){
						Integer year = it.next();
						Map<String, Integer> wordSum = wordYearlySum.get(year);
						SortedMap<String, Integer> ranking = new TreeMap<String, Integer>(new MapComparator(wordSum));
						ranking.putAll(wordSum);
						
						Iterator<String> rankingIt = ranking.keySet().iterator();
						
						for (int i = 0; i < 3 && rankingIt.hasNext(); i++) {
							String word = rankingIt.next();
							writer.println( word + "," + year
									+ "," + wordSum.get(word));

							
							//Contagem ao longo dos 12 meses
							String key = word+"|"+year;
							int[] monthlyCount = wordYearlyCount.get(key);
							if (monthlyCount != null) {

								for (int j = 0; j < monthlyCount.length; j++) {
									writer.print(monthlyCount[j] + ",");
								}
							}
							writer.println();
						}
						writer.println();
					}
					writer.close();
					isFileWritten = true;
					wordCountBoltStopped = true;
				}

			} else {
				String word = tuple.getString(0).toLowerCase();
				Integer year = tuple.getInteger(1);
				int month = tuple.getInteger(2);
				// Key do hashmap composta por word|year
				
				String key = word + "|" + year;
				
				//Insere a contagem daquela palavra relativa aquele mes
				int[] monthlyCount = wordYearlyCount.get(key);
				if (monthlyCount == null) {
					monthlyCount = new int[12];
					monthlyCount[month - 1] = 1;
					wordYearlyCount.put(key, monthlyCount);

				} else {
					monthlyCount[month - 1]++;
				}

				// Cria ou actualiza a contagem global das palavras por ano.
				Map<String, Integer> yearlyCount = wordYearlySum.get(year);

				if (yearlyCount == null) {
					yearlyCount = new HashMap<String, Integer>();
					yearlyCount.put(word, 1);
					wordYearlySum.put(year, yearlyCount );
					

				} else {
					Integer wordCount = yearlyCount.get(word);
					if(wordCount == null)
						yearlyCount.put(word, 1);
					else
						yearlyCount.put(word, wordCount+1);

				}

			}
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("stopSignal"));
		}
	}

	public static class SplitWordsByRegion extends BaseBasicBolt {
		GeoCoderContinents geo = new GeoCoderContinents();

		@Override
		public void execute(Tuple tuple, BasicOutputCollector collector) {

			if (tuple.getValue(0) == null) {
				collector.emit(new Values((Object) null));
			} else {
				// Gets the latitude and longitude values from the tuple
				double latitude = tuple.getDouble(2);
				double longitude = tuple.getDouble(3);
				// Uses GeoCoder to calculate the origin of the tweet
				String region = geo.getContinent(latitude, longitude);

				collector.emit(new Values(region));
			}
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("region"));
		}
	}

	/**
	 * Conta as palavras por Região. No final faz dump para um ficheiro.
	 */
	public static class CountWordsByRegion extends BaseBasicBolt {
		Map<String, Integer> wordRegionCount = new HashMap<String, Integer>();

		int total = 0;

		boolean isFileWritten = false;
		PrintWriter writer;

		@Override
		public void execute(Tuple tuple, BasicOutputCollector collector) {

			if (tuple.getValue(0) == null) {
				if (!isFileWritten) {
					try {
						writer = new PrintWriter("outputRegion.csv", "UTF-8");
					} catch (Exception e) {
					}

					Iterator<String> it = wordRegionCount.keySet().iterator();
					writer.println("sep=,");
					writer.println("Region" + "," + "Count");

					while (it.hasNext()) {
						String key = it.next();
						writer.println();
						writer.println(key + "," + wordRegionCount.get(key));
					}
					writer.println();
					writer.println("Total" + "," + total);

					writer.close();
					isFileWritten = true;
					
					regionBoltStopped = true;
				}

			} else {
				String region = tuple.getString(0);

				// Cria ou actualiza a contagem global das palavras por ano.
				Integer yearlyCount = wordRegionCount.get(region);
				if (yearlyCount == null) {
					wordRegionCount.put(region, 1);
				} else {
					int regionCount = wordRegionCount.get(region);
					wordRegionCount.put(region, regionCount + 1);
				}
				total++;

			}
		}

		@Override
		public void declareOutputFields(OutputFieldsDeclarer declarer) {
			declarer.declare(new Fields("stopSignal"));
		}
	}

	public static void main(String[] args) throws Exception {


		TopologyBuilder builder = new TopologyBuilder();
		builder.setSpout("tweet", new DataExtractorSpout(), 1);

		 builder.setBolt("split", new SplitSentence(), 1).fieldsGrouping(
		 "tweet", new Fields("tweet"));

		builder.setBolt("date", new SplitWordsByDate(), 4).fieldsGrouping(
				"split", new Fields("word"));

		builder.setBolt("counter", new CountWordsByYear(), 2).fieldsGrouping(
				"date", new Fields("word", "year"));

		builder.setBolt("geoCoder", new SplitWordsByRegion(), 4)
				.fieldsGrouping("split", new Fields("word"));

		builder.setBolt("regionCounter", new CountWordsByRegion(), 2)
				.fieldsGrouping("geoCoder", new Fields("region"));

		Config conf = new Config();
		conf.setDebug(true);

		if (args != null && args.length > 0) {
			conf.setNumWorkers(3);

			StormSubmitter.submitTopology(args[0], conf,
					builder.createTopology());
		} else {
			conf.setMaxTaskParallelism(3);

			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology("word-count", conf, builder.createTopology());

			// Testar se é para terminar
			while (!wordCountBoltStopped || !regionBoltStopped) {
				Thread.sleep(1000);
			}
			cluster.shutdown();
		}
	}
}
