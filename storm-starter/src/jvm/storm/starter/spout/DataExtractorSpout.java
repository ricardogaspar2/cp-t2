package storm.starter.spout;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import java.io.FileNotFoundException;
import java.text.ParseException;

import java.util.Map;

import storm.starter.*;

public class DataExtractorSpout extends BaseRichSpout {

	SpoutOutputCollector _collector;
	DataExtractor de;
	long startTime = System.currentTimeMillis();

	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {
		_collector = collector;
		try {
			de = new DataExtractor(
					"//Users/ricardo/Dropbox/FCT-LEI/3ANO/CP/workspace/cpt2/recursos/checkin_data_357.txt");
		} catch (FileNotFoundException e) {
			System.out.println(" FICHEIRO INVÁLIDO!");
		}
	}

	@Override
	public void nextTuple() {
		try {
			Tweet tweet = de.readTweet();
			if (tweet == null){
			}
			// quando for null é porque terminou
			_collector.emit(new Values(tweet));

		} catch (ParseException e) {
			System.out.println("\n\n\n\nERRO NO PARSING!\n\n");
		}

	}

	@Override
	public void ack(Object id) {
	}

	@Override
	public void fail(Object id) {
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tweet"));
	}

}