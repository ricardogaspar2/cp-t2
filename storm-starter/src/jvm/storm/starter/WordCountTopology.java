package storm.starter;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.task.ShellBolt;
import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import storm.starter.spout.RandomSentenceSpout;

import java.util.HashMap;
import java.util.Map;

/**
 * This topology demonstrates Storm's stream groupings and multilang capabilities.
 */

//Ideia 3 níveis de bolts: 
//1) parte frases em palavras e emite-as. 
//2) Separa a primeira letra de cada palavra e emite-a
//3)Contas as letras
public class WordCountTopology {

//Nosso SplitSentence em java. Responsavel por 1)
  public static class SplitSentence extends BaseBasicBolt {

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) { //TODO
      String sentence = tuple.getString(0);
      String[] words = sentence.split(" ");

      for( int i=0; i< words.length; i++){
        collector.emit(new Values(words[i]));
      }

      
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("word"));
    }
  }

    public static class SplitWord extends BaseBasicBolt {

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
      String word = tuple.getString(0);
      char firstLetter = word.charAt(0);

      collector.emit(new Values("" + firstLetter));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("firstletter"));
    }
  }

  public static class LetterCount extends BaseBasicBolt {
    Map<Character, Integer> counts = new HashMap<Character, Integer>();

    @Override
    public void execute(Tuple tuple, BasicOutputCollector collector) {
      char letter = tuple.getString(0).charAt(0);
      Integer count = counts.get(letter);
      if (count == null)
        count = 0;
      count++;
      counts.put(letter, count);
      collector.emit(new Values(letter, count));
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("word", "count"));
    }
  }

  public static void main(String[] args) throws Exception {

    TopologyBuilder builder = new TopologyBuilder();

    builder.setSpout("spout", new RandomSentenceSpout(), 1);

    builder.setBolt("split", new SplitSentence(), 2).shuffleGrouping("spout");
    builder.setBolt("letter", new SplitWord(), 3).fieldsGrouping("split", new Fields("word"));
    builder.setBolt("count", new LetterCount(), 4).fieldsGrouping("letter", new Fields("firstletter"));


    Config conf = new Config();
    conf.setDebug(true);


    if (args != null && args.length > 0) {
      conf.setNumWorkers(3);

      StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
    }
    else {
      conf.setMaxTaskParallelism(3);

      LocalCluster cluster = new LocalCluster();
      cluster.submitTopology("word-count", conf, builder.createTopology());

      Thread.sleep(10000);

      cluster.shutdown();
    }
  }
}
