package storm.starter;

import java.io.Serializable;



public class GeoCoderContinents implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//Contants String to return result
	private static final String EUROPE_STRING = "Europe";
	private static final String NORTH_AMERICA_STRING = "North America";
	private static final String SOUTH_AMERICA_STRING = "South America";
	private static final String AFRICA_STRING = "Africa";
	private static final String OCEANIA_STRING = "Oceania";
	private static final String ASIA_STRING = "Asia";
	private static final String ANTARTICA_STRING = "Antartica";
	private static final String WATER_STRING = "Water";
	
	//Enum with the boundingbox values of every continent.
	public enum Continents {

		EUROPE(34.4802 , 81.4824, -31.13, 69.02),
		NORTH_AMERICA(5.318, 83.40, -168.55, -55.3715 ),
		SOUTH_AMERICA(-59.2920, 13.2403, -92.0033, -26.167),
		AFRICA(-34.5115, 37.2, -25.25, 63.30),
		OCEANIA(-52.908902, -2.460181, 111.782225, 179.443356),
		ASIA(-10.698394, 81.52, 26.4, 170),
		ANTARTICA(-90, -63.1248, -180, 180);
		
		private final double latMin;   
	    private final double latMax; 
	    private final double lonMin; 
	    private final double lonMax; 
	    
	    Continents(double latMin, double latMax, double lonMin, double lonMax) {
	    	this.latMin = latMin;
	        this.latMax = latMax;
	        this.lonMin = lonMin;
	        this.lonMax = lonMax;
	    }
	    
	}
	
	/**
	 * Receive the coordinates latitude and longitude, and calculates the region 
	 * were these coordinates are from.
	 * 
	 * @param lat - Latitude value of the coordenate to test
	 * @param lon - Longitude value of the coordenate to test
	 * 
	 * @return a String with the name of the continent of the values to test.
	 */
	public String getContinent(double lat, double lon) {
		if (lat >= Continents.EUROPE.latMin && lat <= Continents.EUROPE.latMax
				&& lon >= Continents.EUROPE.lonMin && lon <= Continents.EUROPE.lonMax) {
			return EUROPE_STRING;
			
		}else if (lat >= Continents.NORTH_AMERICA.latMin && lat <= Continents.NORTH_AMERICA.latMax
				&& lon >= Continents.NORTH_AMERICA.lonMin && lon <= Continents.NORTH_AMERICA.lonMax) {
			return NORTH_AMERICA_STRING;
			
		}else if (lat >= Continents.SOUTH_AMERICA.latMin && lat <= Continents.SOUTH_AMERICA.latMax
				&& lon >= Continents.SOUTH_AMERICA.lonMin && lon <= Continents.SOUTH_AMERICA.lonMax) {
			return SOUTH_AMERICA_STRING;
			
		}else if (lat >= Continents.AFRICA.latMin && lat <= Continents.AFRICA.latMax
				&& lon >= Continents.AFRICA.lonMin && lon <= Continents.AFRICA.lonMax) {
			return AFRICA_STRING;
		
		}else if (lat >= Continents.ASIA.latMin && lat <= Continents.ASIA.latMax
				&& lon > Continents.ASIA.lonMin && lon < Continents.ASIA.lonMax) {
			return ASIA_STRING;
			
		}else if (lat >= Continents.OCEANIA.latMin && lat <= Continents.OCEANIA.latMax
				&& lon >= Continents.OCEANIA.lonMin && lon <= Continents.OCEANIA.lonMax) {
			return OCEANIA_STRING;
			
		}else if (lat >= Continents.ANTARTICA.latMin && lat <= Continents.ANTARTICA.latMax
				&& lon >= Continents.ANTARTICA.lonMin && lon <= Continents.ANTARTICA.lonMax) {
			return ANTARTICA_STRING;
		}
		
		return WATER_STRING;
	}
}

